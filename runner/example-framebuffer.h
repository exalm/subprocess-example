#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define EXAMPLE_TYPE_FRAMEBUFFER (example_framebuffer_get_type())

G_DECLARE_FINAL_TYPE (ExampleFramebuffer, example_framebuffer, EXAMPLE, FRAMEBUFFER, GObject)

ExampleFramebuffer *example_framebuffer_new (gsize width,
                                             gsize height,
                                             gsize depth);

G_END_DECLS
