#pragma once

#include <glib-2.0/glib.h>

G_BEGIN_DECLS

gint example_memfd_create (const gchar *name);

G_END_DECLS
