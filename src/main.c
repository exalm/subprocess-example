#include <gtk/gtk.h>

#include "example-window.h"

static void
activate_cb (GtkApplication *app,
             gpointer        user_data)
{
  GtkWindow *window;

  window = gtk_application_get_active_window (app);

  if (!window)
    window = example_window_new (app);

  gtk_window_present (window);
}

gint
main (gint    argc,
      gchar **argv)
{
  GtkApplication *app;

  app = gtk_application_new ("org.example.App", G_APPLICATION_FLAGS_NONE);

  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}

