#pragma once

#include <glib-object.h>

#include "ipc-runner.h"

G_BEGIN_DECLS

#define EXAMPLE_TYPE_SUBPROCESS (example_subprocess_get_type())

G_DECLARE_FINAL_TYPE (ExampleSubprocess, example_subprocess, EXAMPLE, SUBPROCESS, GObject)

ExampleSubprocess *example_subprocess_new (void);

IpcRunner *example_subprocess_start (ExampleSubprocess  *self,
                                     GError            **error);
void example_subprocess_stop (ExampleSubprocess  *self,
                              GError            **error);

G_END_DECLS
