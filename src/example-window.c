#include "example-window.h"

#include "example-subprocess.h"
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gio/gunixfdlist.h>
#include <math.h>

struct _ExampleWindow
{
  GtkApplicationWindow parent_instance;

  ExampleSubprocess *subprocess;
  IpcRunner *proxy;
  GdkPixbuf *pixbuf;
  gsize width;
  gsize height;

  GtkWidget *area;
  GtkLabel *label;
  GtkLabel *fps_label;
  GtkStack *stack;
  GtkRevealer *revealer;
  GtkButton *start_btn;
  GtkButton *stop_btn;
  GtkToggleButton *pause_btn;
  guint64 ticks;
  guint fps_timeout_id;
};

G_DEFINE_TYPE (ExampleWindow, example_window, GTK_TYPE_APPLICATION_WINDOW)

static gboolean
fps_timeout_cb (ExampleWindow *self)
{
  g_autofree gchar *fps_string = g_strdup_printf ("%lu FPS", self->ticks);
  gtk_label_set_label (self->fps_label, fps_string);
  self->ticks = 0;

  return G_SOURCE_CONTINUE;
}

static void
start_fps_counter (ExampleWindow *self)
{
  g_assert (!self->fps_timeout_id);

  self->ticks = 0;
  fps_timeout_cb (self);
  self->fps_timeout_id = g_timeout_add_full (G_PRIORITY_DEFAULT_IDLE, 1000,
                                             (GSourceFunc) fps_timeout_cb,
                                             self, NULL);

  gtk_widget_show (GTK_WIDGET (self->fps_label));
}

static void
stop_fps_counter (ExampleWindow *self)
{
  g_assert (self->fps_timeout_id);

  g_source_remove (self->fps_timeout_id);
  self->fps_timeout_id = 0;

  gtk_widget_hide (GTK_WIDGET (self->fps_label));
}

static gchar *
get_time_string (int time)
{
  return g_strdup_printf ("Time elapsed: %.3f seconds", time / 62.5);
}

static void
free_pixbuf (guchar *pixels, gpointer data)
{
  g_free (pixels);
}

static void
tick_cb_complete (GDBusProxy    *proxy,
                  GAsyncResult  *res,
                  ExampleWindow *self)
{
  g_autoptr(GError) error = NULL;
  g_autoptr(GVariant) frame = NULL;
  g_autoptr(GUnixFDList) out_fd_list = NULL;
  gint fd, handle;
  gsize stride;
  guchar *data;

  frame = g_dbus_proxy_call_with_unix_fd_list_finish (proxy, &out_fd_list, res,
                                                      &error);

  if (!self->proxy)
    return;

  if (error)
    g_critical ("Couldn't get frame: %s", error->message);

  g_variant_get (frame, "(th)", &stride, &handle);
  if (handle < g_unix_fd_list_get_length (out_fd_list)) {
    fd = g_unix_fd_list_get (out_fd_list, handle, &error);
    if (error)
      g_critical ("Couldn't get fd: %s", error->message);
  } else {
    g_critical ("Invalid handle");
    return;
  }

  data = g_new (guchar, self->height * stride);
  read (fd, data, self->height * stride);
  close (fd);

  if (self->pixbuf)
    g_object_unref (self->pixbuf);
  self->pixbuf = gdk_pixbuf_new_from_data (data, GDK_COLORSPACE_RGB, TRUE, 8,
                                           self->width, self->height, stride,
                                           free_pixbuf, NULL);
  gtk_widget_queue_draw (self->area);

  self->ticks++;
}

static void
tick_cb (ExampleWindow *self,
         gint           time,
         IpcRunner     *proxy)
{
  g_autofree gchar *str = NULL;

  str = get_time_string (time);
  gtk_label_set_label (self->label, str);

  g_dbus_proxy_call_with_unix_fd_list (G_DBUS_PROXY (proxy), "GetFrame",
                                       g_variant_new ("(tt)", self->width, self->height),
                                       G_DBUS_CALL_FLAGS_NONE, -1, NULL, NULL,
                                       (GAsyncReadyCallback) tick_cb_complete,
                                       self);
}

static void
exit_cb (ExampleSubprocess *subprocess,
         gboolean           success,
         gint               exit_code,
         ExampleWindow     *self)
{
  g_signal_handlers_disconnect_by_func (self->proxy, tick_cb, self);
  g_clear_object (&self->proxy);

  gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->start_btn));
  gtk_revealer_set_reveal_child (self->revealer, FALSE);
  gtk_toggle_button_set_active (self->pause_btn, FALSE);
  if (success)
    gtk_label_set_label (self->label, "Disconnected");
  else {
    g_autofree gchar *str = NULL;
    str = g_strdup_printf ("Oops! The subprocess exited with code %d", exit_code);
    gtk_label_set_label (self->label, str);
  }

  stop_fps_counter (self);

  g_clear_object (&self->pixbuf);
  gtk_widget_queue_draw (self->area);
}

static void
start_clicked_cb (GtkButton     *button,
                  ExampleWindow *self)
{
  g_autoptr(GError) error = NULL;

  self->proxy = example_subprocess_start (self->subprocess, &error);
  if (error) {
    g_warning ("Couldn't connect: %s", error->message);
    return;
  }

  g_assert_nonnull (self->proxy);

  g_signal_connect_swapped (self->proxy, "tick", G_CALLBACK (tick_cb), self);

  gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->stop_btn));
  gtk_revealer_set_reveal_child (self->revealer, TRUE);

  start_fps_counter (self);
}

static void
stop_clicked_cb (GtkButton     *button,
                 ExampleWindow *self)
{
  g_autoptr(GError) error = NULL;

  g_assert_nonnull (self->proxy);

  example_subprocess_stop (self->subprocess, &error);
  if (error)
    g_warning ("Couldn't disconnect: %s", error->message);
}

static void
pause_toggled_cb (GtkButton     *button,
                  ExampleWindow *self)
{
  g_autoptr(GError) error = NULL;

  g_assert_nonnull (self->proxy);

  if (gtk_toggle_button_get_active (self->pause_btn)) {
    if (!ipc_runner_call_pause_sync (self->proxy, NULL, &error))
      g_warning ("Couldn't pause: %s", error->message);

    stop_fps_counter (self);
  } else {
    gint time = 0;
    g_autofree gchar *str = NULL;

    if (!ipc_runner_call_unpause_sync (self->proxy, &time, NULL, &error)) {
      g_warning ("Couldn't unpause: %s", error->message);
      return;
    }

    str = get_time_string (time);

    gtk_label_set_label (self->label, str);

    gtk_widget_show (GTK_WIDGET (self->fps_label));

    start_fps_counter (self);
  }
}

static gboolean
draw_cb (GtkWidget     *area,
         cairo_t       *cr,
         ExampleWindow *self)
{
  if (self->pixbuf) {
    gdk_cairo_set_source_pixbuf(cr, self->pixbuf, 0, 0);
    cairo_paint (cr);
  }

  return GDK_EVENT_PROPAGATE;
}

static void
size_allocate_cb (GtkWidget     *widget,
                  GtkAllocation *allocation,
                  ExampleWindow *self)
{
  self->width = allocation->width;
  self->height = allocation->height;
}

static void
example_window_finalize (GObject *object)
{
  ExampleWindow *self = (ExampleWindow *)object;

  if (self->pixbuf)
    g_object_unref (self->pixbuf);

  if (self->fps_timeout_id) {
    g_source_remove (self->fps_timeout_id);
    self->fps_timeout_id = 0;
  }

  g_clear_object (&self->proxy);
  g_clear_object (&self->subprocess);

  G_OBJECT_CLASS (example_window_parent_class)->finalize (object);
}

static void
example_window_class_init (ExampleWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = example_window_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/App/example-window.ui");
  gtk_widget_class_bind_template_child (widget_class, ExampleWindow, area);
  gtk_widget_class_bind_template_child (widget_class, ExampleWindow, label);
  gtk_widget_class_bind_template_child (widget_class, ExampleWindow, fps_label);
  gtk_widget_class_bind_template_child (widget_class, ExampleWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, ExampleWindow, revealer);
  gtk_widget_class_bind_template_child (widget_class, ExampleWindow, start_btn);
  gtk_widget_class_bind_template_child (widget_class, ExampleWindow, stop_btn);
  gtk_widget_class_bind_template_child (widget_class, ExampleWindow, pause_btn);
  gtk_widget_class_bind_template_callback (widget_class, start_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, stop_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, pause_toggled_cb);
  gtk_widget_class_bind_template_callback (widget_class, draw_cb);
  gtk_widget_class_bind_template_callback (widget_class, size_allocate_cb);
}

static void
example_window_init (ExampleWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->subprocess = example_subprocess_new ();
  g_signal_connect_object (self->subprocess, "exit", G_CALLBACK (exit_cb), self, 0);
}

GtkWindow *
example_window_new (GtkApplication *app)
{
  return g_object_new (EXAMPLE_TYPE_WINDOW, "application", app, NULL);
}

